# SMTP network load balancer

resource "aws_lb" "nlb" {
  name = "maildrop-smtp-lb"
  internal = false
  load_balancer_type = "network"
  subnets = ["${aws_subnet.public.*.id}"]
}

# Target group for SMTP connections

resource "aws_lb_target_group" "backend" {
  name = "maildrop-smtp-target"
  port = 25
  protocol = "TCP"
  vpc_id = "${aws_vpc.maildrop.id}"
  target_type = "instance"
}

# Listener on port 25

resource "aws_lb_listener" "frontend" {
  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.backend.arn}"
  }
  protocol = "TCP"
  load_balancer_arn = "${aws_lb.nlb.arn}"
  port = 25
}

output "mx" {
  value = "${aws_lb.nlb.dns_name}"
}
